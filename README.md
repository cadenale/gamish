# gamish

A project to learn about video game programming with functional reactive programming.
Currently it demonstrates some path-finding algorithms.

## Credits
Some assets are not created by me and are distributed under a different licence:

"basictiles.png" and "characters.png" were created by Lanea Zimmerman and distributed under CC-By 3.0 and OGA-BY 3.0.
https://opengameart.org/content/tiny-16-basic

"data/Cave-Story.ttf" was created by enigmansmp1824 and distributed  under CC-BY-SA 3.0.
https://fontlibrary.org/en/font/cave-story

{-

-}

module Actor where


import qualified Data.Vector as V
import Linear 


data Actor = Actor
    { textureOffset :: V2 Int
    , position :: V2 Float
    , speed :: Float
    , direction :: Direction
    , state :: State
    , frameCount :: Int
    } deriving (Read, Show, Eq)


data State = 
      MoveTo [V2 Float]
    | StandBy
    deriving (Read, Show , Eq)


data Direction = North | East | South | West
    deriving (Read, Show, Eq)


humanMale :: Actor
humanMale = Actor (V2 48 0) (V2 0 0) 0.2 South StandBy 4


keyframesX :: V.Vector Int
keyframesX = V.fromList [0, 16, 32, 16]


getKeyframeX :: Int -> Int
getKeyframeX n = keyframesX V.! (n `quot` 4)

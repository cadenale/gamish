{-

-}
module Sprite where


import Control.Monad.IO.Class
import SDL
import qualified SDL.Font as F
import SDL.Image

data Sprites = Sprites
    { renderer :: Renderer
    , objects :: Texture
    , actors :: Texture
    , buttons :: Texture
    , breadth :: Texture
    , dijkstra :: Texture
    , greedy :: Texture
    , aStar :: Texture
    }


loadSprites :: MonadIO m => Renderer -> m Sprites
loadSprites renderer0 = do
    let color = V4 0xDE 0xEE 0xD6 0xFF
    F.initialize
    font <- F.load "data/Cave-Story.ttf" 24
    bfs <- F.solid font color "Breadth "
    djs <- F.solid font color "Dijkstra"
    grs <- F.solid font color " Greedy "
    a's <- F.solid font color "   A*   "
    sprites <- Sprites renderer0
        <$> loadTexture renderer0 "data/basictiles.png"
        <*> loadTexture renderer0 "data/characters.png"
        <*> loadTexture renderer0 "data/play-stop-buttons.png"
        <*> createTextureFromSurface renderer0 bfs
        <*> createTextureFromSurface renderer0 djs
        <*> createTextureFromSurface renderer0 grs
        <*> createTextureFromSurface renderer0 a's
    F.free font
    freeSurface bfs
    freeSurface djs
    freeSurface grs
    freeSurface a's
    F.quit
    return sprites
    


destroySprites :: MonadIO m => Sprites -> m ()
destroySprites sprites = 
    mapM_
        (\f -> destroyTexture $ f sprites)
        [objects, actors, buttons, breadth, dijkstra, greedy, aStar]
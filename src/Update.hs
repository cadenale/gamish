{-

-}

module Update where


import qualified Actor as A
import Control.Monad
import Control.Monad.IO.Class
import Data.Function
import Data.Functor
import Data.List
import qualified Data.Map.Strict as M
import Data.Maybe
import Linear
import qualified Data.Vector as V
import qualified World as W


updateState :: MonadIO m => W.World -> m W.GameState
updateState W.World
    { W.worldMap = layer
    , W.goal = goal
    , W.gameState = W.BreadthFirst 
        { W.frontier = frontier
        , W.cameFrom = cameFrom 
        }
    } = liftIO $ updateBreadthFirst layer goal frontier cameFrom

updateState W.World
    { W.worldMap = layer
    , W.goal = goal
    , W.gameState = W.Dijkstra 
        { W.costFrontier = frontier
        , W.costFrom = cameFrom 
        }
    } = liftIO $ updateDijkstra layer goal frontier cameFrom

updateState W.World
    { W.worldMap = layer
    , W.goal = goal
    , W.gameState = W.GreedyBest 
        { W.costFrontier = frontier
        , W.costFrom = cameFrom 
        }
    } = liftIO $ updateGreedyBest layer goal frontier cameFrom

updateState W.World
    { W.worldMap = layer
    , W.goal = goal
    , W.gameState = W.AStar
        { W.costFrontier = frontier
        , W.checkFrom = cameFrom 
        }
    } = liftIO $ updateAStar layer goal frontier cameFrom

updateState W.World { W.gameState = state } = return state


updateBreadthFirst 
    :: W.Layer W.Tile
    -> V2 Int 
    -> [V2 Int] 
    -> W.FreeExploration
    -> IO W.GameState
updateBreadthFirst _ _ [] cameFrom =
    return $ W.NoPath $ V.fromList $ M.keys cameFrom

updateBreadthFirst _ goal _ cameFrom | isJust $ cameFrom M.!? goal = 
        return $ W.Found visited path where
            visited = V.fromList $ M.keys cameFrom
            path = V.fromList $ reverse $ unfoldr retro $ Just goal
            retro :: Maybe (V2 Int) -> Maybe (V2 Int, Maybe (V2 Int))
            retro mv = do
                v <- mv
                w <- cameFrom M.!? v
                return (v, w)

updateBreadthFirst layer _ (current@(V2 x0 y0) : unex) cameFrom = do 
    let isPath v = (Just W.Path ==) <$> W.readLayer layer v
    neightbors <- filterM isPath $ 
        [ V2 (x0 + 1) y0
        , V2 x0 (y0 + 1)
        , V2 (x0 - 1) y0
        , V2 x0 (y0 - 1)
        ]
    let nextOnes = filter (isNothing . (cameFrom M.!?)) neightbors
        cf = foldr (\v m -> M.insert v (Just current) m) cameFrom nextOnes
    return $ W.BreadthFirst (unex ++ nextOnes) cf

        
updateDijkstra
    :: W.Layer W.Tile
    -> V2 Int 
    -> [(Float, V2 Int)] 
    -> W.Checked Float
    -> IO W.GameState
updateDijkstra _ _ [] cameFrom =
    return $ W.NoPath $ V.fromList $ M.keys cameFrom

updateDijkstra _ goal _ cameFrom | isJust $ cameFrom M.!? goal = 
    return $ W.Found visited path where
        visited = V.fromList $ M.keys cameFrom
        path = V.fromList $ reverse $ unfoldr retro $ Just goal
        retro :: Maybe (V2 Int) -> Maybe (V2 Int, Maybe (V2 Int))
        retro mv = do
            v <- mv
            (_, w) <- cameFrom M.!? v
            return (v, w)

updateDijkstra layer _ ((c0, current@(V2 x0 y0)) : unex) cameFrom = do 
    let getPath :: V2 Int -> Float -> IO (Maybe (Float, V2 Int))
        getPath v cost = do
            t <- W.readLayer layer v
            return $ guard (Just W.Path == t) $> (c0 + cost, v)
        corner :: Maybe a -> Maybe b -> V2 Int -> IO (Maybe (Float, V2 Int))
        corner p1 p2 v = (p1 *> p2 *>) <$> getPath v (sqrt 2)
        isNext :: (Float, V2 Int) -> Bool
        isNext (cost, v) = case cameFrom M.!? v of
            Nothing -> True
            Just (costOld, _) -> cost < costOld 
    neightbors <- do
        e <- getPath (V2 (x0 + 1) y0) 1
        s <- getPath (V2 x0 (y0 + 1)) 1
        w <- getPath (V2 (x0 - 1) y0) 1
        n <- getPath (V2 x0 (y0 - 1)) 1
        se <- corner s e (V2 (x0 + 1) (y0 + 1))
        sw <- corner s w (V2 (x0 - 1) (y0 + 1))
        nw <- corner n w (V2 (x0 - 1) (y0 - 1))
        ne <- corner n e (V2 (x0 + 1) (y0 - 1))
        return $ catMaybes [n, e, s, w, se, sw, nw, ne]
    let nextOnes = filter isNext neightbors
        ins (c, v) m = M.insert v (c, Just current) m
        cf = foldr ins cameFrom nextOnes
    return $ W.Dijkstra (sortBy (compare `on` fst) $ unex ++ nextOnes) cf

-- Greedy best first
updateGreedyBest
    :: W.Layer W.Tile
    -> V2 Int 
    -> [(Float, V2 Int)] 
    -> W.Checked Float
    -> IO W.GameState
updateGreedyBest _ _ [] cameFrom =
    return $ W.NoPath $ V.fromList $ M.keys cameFrom

updateGreedyBest _ goal _ cameFrom | isJust $ cameFrom M.!? goal = 
    return $ W.Found visited path where
        visited = V.fromList $ M.keys cameFrom
        path = V.fromList $ reverse $ unfoldr retro $ Just goal
        retro :: Maybe (V2 Int) -> Maybe (V2 Int, Maybe (V2 Int))
        retro mv = do
            v <- mv
            (_, w) <- cameFrom M.!? v
            return (v, w)

updateGreedyBest layer goal ((_, current@(V2 x0 y0)) : unex) cameFrom = do 
    let getPath :: V2 Int -> IO (Maybe (Float, V2 Int))
        getPath v = do
            t <- W.readLayer layer v
            return $ guard (Just W.Path == t) $> 
                (norm $ fromIntegral <$> goal - v, v)
        isNext :: (Float, V2 Int) -> Bool
        isNext (cost, v) = case cameFrom M.!? v of
            Nothing -> True
            Just (costOld, _) -> cost < costOld 
    neightbors <- catMaybes <$> sequenceA
        [ getPath (V2 (x0 + 1) y0)
        , getPath (V2 x0 (y0 + 1))
        , getPath (V2 (x0 - 1) y0)
        , getPath (V2 x0 (y0 - 1))
        ]
    let nextOnes = filter isNext neightbors
        ins (c, v) m = M.insert v (c, Just current) m
        cf = foldr ins cameFrom nextOnes
    return $ W.GreedyBest (sortBy (compare `on` fst) $ unex ++ nextOnes) cf
-- end greedy best first
-- A Star
updateAStar
    :: W.Layer W.Tile
    -> V2 Int 
    -> [(Float, V2 Int)] 
    -> W.Checked (V2 Float)
    -> IO W.GameState
updateAStar _ _ [] cameFrom =
    return $ W.NoPath $ V.fromList $ M.keys cameFrom

updateAStar _ goal _ cameFrom | isJust $ cameFrom M.!? goal = 
    return $ W.Found visited path where
        visited = V.fromList $ M.keys cameFrom
        path = V.fromList $ reverse $ unfoldr retro $ Just goal
        retro :: Maybe (V2 Int) -> Maybe (V2 Int, Maybe (V2 Int))
        retro mv = do
            v <- mv
            (_, w) <- cameFrom M.!? v
            return (v, w)

updateAStar layer goal ((_, current@(V2 x0 y0)) : unex) cameFrom = do 
    let getPath :: V2 Int -> Float -> IO (Maybe (Float, V2 Int))
        V2 _ c0 = maybe 0 fst (cameFrom M.!? current)
        getPath v cost = do
            t <- W.readLayer layer v
            return $ guard (Just W.Path == t) $> (c0 + cost, v)
        corner :: Maybe a -> Maybe b -> V2 Int -> IO (Maybe (Float, V2 Int))
        corner p1 p2 v = (p1 *> p2 *>) <$> getPath v (sqrt 2)
        isNext :: (Float, V2 Int) -> Maybe (V2 Float, V2 Int)
        isNext (cost, v) = case cameFrom M.!? v of
            Nothing ->
                Just (V2 (norm $ fromIntegral <$> goal - v) cost, v)
            Just (V2 d costOld, _) | cost < costOld -> Just (V2 d cost, v)
            _ -> Nothing
    neightbors <- do
        e <- getPath (V2 (x0 + 1) y0) 1
        s <- getPath (V2 x0 (y0 + 1)) 1
        w <- getPath (V2 (x0 - 1) y0) 1
        n <- getPath (V2 x0 (y0 - 1)) 1
        se <- corner s e (V2 (x0 + 1) (y0 + 1))
        sw <- corner s w (V2 (x0 - 1) (y0 + 1))
        nw <- corner n w (V2 (x0 - 1) (y0 - 1))
        ne <- corner n e (V2 (x0 + 1) (y0 - 1))
        return $ catMaybes [n, e, s, w, se, sw, nw, ne]
    let nextOnes = catMaybes $ isNext <$> neightbors
        ins (vf, v) m = M.insert v (vf, Just current) m
        cf = foldr ins cameFrom nextOnes
        nextP = (\(V2 d c, v) -> (d + c, v)) <$> nextOnes
        unexP = filter (\(_, v) -> not $ any (\(_, w) -> v == w) nextP) unex
    return $ W.AStar (sortBy (compare `on` fst) $ unexP ++ nextP) cf
-- End A Star

updateExplorer :: A.Actor -> V2 Int -> V.Vector (V2 Int) -> A.Actor
updateExplorer actor@A.Actor { A.state = A.StandBy } start path =
    actor 
        { A.position = fromIntegral <$> start
        , A.state = A.MoveTo $ ((fromIntegral) <$>) <$> V.toList path 
        }

updateExplorer 
    actor@A.Actor 
        { A.speed = speed
        , A.position = position0
        , A.state = A.MoveTo targets
        -- , A.direction = direction0
        , A.frameCount = frame 
        } _ _ = let
            (position1, state1) = advanceExplorer speed position0 targets
            V2 x1 y1 = position1 - position0
            delta = abs x1 - abs y1 
            direction =  if nearZero delta || delta > 0
                then (if x1 > 0 then A.East  else A.West)
                else (if y1 > 0 then A.South else A.North)     
            in actor 
                { A.position = position1
                , A.state = state1
                , A.direction = direction
                , A.frameCount = (frame + 1) `rem` 16
                }


advanceExplorer :: Float -> V2 Float -> [V2 Float] -> (V2 Float, A.State)
advanceExplorer d position targets@(target:nextTargets) = let
    dv = target - position
    dd = norm dv 
    in if dd > d
        then (position + ((d *) <$> normalize dv), A.MoveTo targets)
        else advanceExplorer (d - dd) target nextTargets

advanceExplorer _ position [] = (position, A.StandBy)


switchState :: W.World -> W.Algorithm -> W.GameState
switchState W.World { W.gameState = W.Edit, W.start = v } W.BreadthFirstA =
    W.BreadthFirst [v] $ M.singleton v Nothing
switchState W.World { W.gameState = W.Edit, W.start = v } W.DijkstraA =
    W.Dijkstra [(0, v)] $ M.singleton v (0, Nothing)
switchState W.World { W.gameState = W.Edit, W.start = v } W.GreedyBestA =
    W.GreedyBest [(0, v)] $ M.singleton v (0, Nothing)
switchState W.World { W.gameState = W.Edit, W.start = v } W.AStarA =
    W.AStar [(0, v)] $ M.singleton v (0, Nothing)
switchState _ _ = W.Edit


switchAlgorithm :: W.Algorithm -> W.Algorithm
switchAlgorithm W.BreadthFirstA = W.DijkstraA
switchAlgorithm W.DijkstraA = W.GreedyBestA
switchAlgorithm W.GreedyBestA = W.AStarA
switchAlgorithm W.AStarA = W.BreadthFirstA


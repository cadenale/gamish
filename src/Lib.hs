{-# LANGUAGE
     OverloadedStrings,
     RecursiveDo
 #-}
module Lib
    ( gamish
    ) where


import qualified Actor as A
import Control.Monad
import Data.Functor
import Draw
import Reactive.Banana
import Reactive.Banana.Frameworks
import SDL hiding (Event)
import qualified Sprite as S
import Update
import qualified World as W


gamish :: IO ()
gamish = do
    initializeAll
    window <- createWindow
        "Gamish"
        defaultWindow 
            { windowOpenGL = Just defaultOpenGL
            , windowInitialSize = V2 640 480
            }
    renderer <- createRenderer window (-1) defaultRenderer 
        { rendererType = AcceleratedVSyncRenderer
        }
    (eventH, pushEvent) <- newAddHandler
    (tickH, pushTick) <- newAddHandler
    sprites <- S.loadSprites renderer
    network <- compile $ makeNetwork eventH tickH sprites
    actuate network
    ticker <- addTimer 33 $ \dt -> do
        pushTick ()
        return $ Reschedule dt
    let loop = do
            event <- eventPayload <$> waitEvent
            pushEvent event
            unless (isQuit event) loop
                
    loop
    void $ removeTimer ticker


makeNetwork
    :: AddHandler EventPayload
    -> AddHandler ()
    -> S.Sprites
    -> MomentIO ()
makeNetwork eventH tickH sprites = mdo
    layer <- liftIO $ W.makeLayer (V2 30 30) W.Path
    event <- fromAddHandler eventH
    tick <- fromAddHandler tickH
    let prim = filterJust $ getMousePress ButtonLeft <$> event
        edit1 b1 b2 v = b1 == b2 && inSquare 30 (V2 0 0) v
        whenEdit = whenE $ gameState <&> (== W.Edit)
        position1 v b = stepper v $ filterApply (edit1 b <$> brush) $
            whenEdit $ ((`quot` 16) <$>) <$> prim
    brush <- stepper W.PathBrush $ filterJust $ selectBrush <$> prim
    algorithm <- stepper W.BreadthFirstA $ whenEdit $
        switchAlgorithm <$> algorithm <@ 
        filterE (inRect 64 32 $ V2 528 208) prim
    s0 <- position1 (V2 0 0) W.StartBrush
    g0 <- position1 (V2 8 8) W.GoalBrush
    gameStateE <- unionWith const
        (switchState <$> world <*> algorithm <@ 
            filterE (inSquare 32 $ V2 544 400) prim) <$>
        mapEventIO updateState (world <@ tick)
    gameState <- stepper W.Edit gameStateE
    explorer <- stepper A.humanMale $ 
        followPath <$> explorer <*> s0 <@> gameStateE
    let world = W.World layer <$> 
            explorer <*> s0 <*> g0 <*> brush <*> algorithm <*> gameState
        stroke :: Event [V2 Int]
        stroke = whenEdit $
            (strokeLine <$> filterJust (getMouseMotion <$> event)) <>
            (pure <$> fmap (`quot` 16) <$> prim)
        change a vs = mapM_ (\v -> W.writeLayer layer v a) vs
    reactimate $ change W.Wall <$> whenE (brush <&> (== W.WallBrush)) stroke
    reactimate $ change W.Path <$> whenE (brush <&> (== W.PathBrush)) stroke
    reactimate $ draw sprites <$> (world <@ tick)


isQuit :: EventPayload -> Bool
isQuit (WindowClosedEvent _) = True
isQuit (KeyboardEvent (KeyboardEventData
        { keyboardEventKeyMotion = Pressed
        , keyboardEventKeysym = Keysym {keysymKeycode = keycode}
        }
    )) = keycode `elem` [KeycodeQ, KeycodeEscape]
isQuit _ = False


selectBrush :: V2 Int -> Maybe W.Brush
selectBrush v 
    | inIcon $ V2 544  16 = Just W.PathBrush
    | inIcon $ V2 544  64 = Just W.WallBrush
    | inIcon $ V2 544 112 = Just W.StartBrush
    | inIcon $ V2 544 160 = Just W.GoalBrush
    | otherwise = Nothing
    where inIcon v0 = inSquare 32 v0 v 


inSquare :: (Ord r, Num r) => r -> V2 r -> V2 r -> Bool
inSquare n = inRect n n


inRect :: (Ord r, Num r) => r -> r -> V2 r -> V2 r -> Bool
inRect n m v0 v = let V2 x y = v - v0 in 0 <= x && x < n && 0 <= y && y < m


getMousePress :: Integral a => MouseButton -> EventPayload -> Maybe (V2 a)
getMousePress expected (MouseButtonEvent (MouseButtonEventData
        { mouseButtonEventMotion = Pressed 
        , mouseButtonEventButton = button
        , mouseButtonEventPos = P v
        }
    )) | expected == button = Just $ fromIntegral <$> v
getMousePress _ _ = Nothing


getMouseMotion :: Num n => EventPayload -> Maybe (V2 n, V2 n)
getMouseMotion (MouseMotionEvent (MouseMotionEventData
        { mouseMotionEventState = buttons
        , mouseMotionEventPos = (P pos)
        , mouseMotionEventRelMotion = rel
        }
    )) | ButtonLeft `elem` buttons = Just $ (r, p) where
        p = fromIntegral <$> pos
        r = fromIntegral <$> pos - rel
getMouseMotion _ = Nothing


strokeLine :: (V2 Int, V2 Int) -> [V2 Int]
strokeLine (vi, vj) = if dm == 0 then [vi] else lerp' dm <$> [0 .. dm]
    where
        wi@(V2 xi yi) = vi <&> (`quot` 16)
        wj@(V2 xj yj) = vj <&> (`quot` 16)
        dm = max (abs $ xi - xj) (abs $ yi - yj)
        lerp' u t = wi + ((`div` u) . (t *) <$> (wj - wi)) 


followPath :: A.Actor -> V2 Int -> W.GameState -> A.Actor
followPath actor start W.Found { W.path = path } = 
    updateExplorer actor start path
followPath _ start _ = A.humanMale { A.position = fromIntegral <$> start }



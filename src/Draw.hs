{-
-}

module Draw (draw) where 
    
import qualified Actor as A
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Map as M
import SDL
import qualified Sprite as S
import qualified World as W


draw :: MonadIO m => S.Sprites -> W.World -> m ()
draw sprites world = do
    drawMap sprites world
    drawGui sprites world
    present $ S.renderer sprites


drawMap :: MonadIO m => S.Sprites -> W.World -> m ()
drawMap sprites@S.Sprites { S.renderer = renderer } world = do
    let wm = W.worldMap world
    sequence_ $ do
        let V2 w h = W.layerSize wm
        x <- [0 .. w - 1]
        y <- [0 .. h - 1]
        return $ liftIO $ do
            copyTile (S.objects sprites) (V2 0 128) $ V2 (x * 16) (y * 16)
            tile <- W.readLayer wm (V2 x y)
            when (tile == Just W.Wall) $
                copyTile (S.objects sprites) (V2 96 64) $ V2 (x * 16) (y * 16)
    drawExploration renderer $ W.gameState world
    drawExplorer renderer (S.actors sprites) $ W.explorer world
    copyTile (S.objects sprites) (V2 64 64) ((16 *) <$> W.goal world)
    where
        copyTile t source target = 
            copy renderer t (tileRect source) (tileRect target)
        


drawExploration :: MonadIO m => Renderer -> W.GameState -> m ()
drawExploration _ W.Edit = return ()

drawExploration 
    renderer 
    W.BreadthFirst { W.frontier = frontier, W.cameFrom = cameFrom } =
        drawExploring renderer frontier (M.keys cameFrom)

drawExploration 
    renderer 
    W.Dijkstra { W.costFrontier = frontier, W.costFrom = cameFrom } =
        drawExploring renderer (snd <$> frontier) (M.keys cameFrom)

drawExploration 
    renderer 
    W.GreedyBest { W.costFrontier = frontier, W.costFrom = cameFrom } =
        drawExploring renderer (snd <$> frontier) (M.keys cameFrom)

drawExploration 
    renderer 
    W.AStar { W.costFrontier = frontier, W.checkFrom = cameFrom } =
        drawExploring renderer (snd <$> frontier) (M.keys cameFrom)

drawExploration renderer W.Found { W.visited = visited, W.path = path } = do
    forM_ visited $ \v -> do
        rendererDrawColor renderer $= V4 0x40 0xC0 0x40 0xFF
        fillRect renderer $ Just 
                $ Rectangle (P $ fromIntegral . (16 *) <$> v) (V2 16 16)

    forM_ path $ \v -> do
        rendererDrawColor renderer $= V4 0x40 0x40 0xC0 0xFF
        fillRect renderer $ Just 
            $ Rectangle (P $ fromIntegral . (16 *) <$> v) (V2 16 16)

drawExploration renderer W.NoPath { W.visited = visited } = do
    forM_ visited $ \v -> do
        rendererDrawColor renderer $= V4 0xC0 0x40 0x40 0xFF
        fillRect renderer $ Just 
                $ Rectangle (P $ fromIntegral . (16 *) <$> v) (V2 16 16)


drawExploring :: MonadIO m => Renderer -> [V2 Int] -> [V2 Int] -> m ()
drawExploring renderer frontier explored  = do
    forM_ explored $ \v -> do
        rendererDrawColor renderer $= V4 0x40 0xC0 0x40 0xFF
        fillRect renderer $ Just 
                $ Rectangle (P $ fromIntegral . (16 *) <$> v) (V2 16 16)

    forM_ frontier $ \v -> do
        rendererDrawColor renderer $= V4 0x40 0x40 0xC0 0xFF
        fillRect renderer $ Just 
            $ Rectangle (P $ fromIntegral . (16 *) <$> v) (V2 16 16)


drawExplorer :: MonadIO m => Renderer -> Texture -> A.Actor -> m ()
drawExplorer renderer sprite A.Actor 
    { A.textureOffset = V2 xf yf
    , A.position = position
    , A.direction = direction
    , A.frameCount = frame
    } = copy renderer sprite (tileRect $ V2 x y) p where
        p = Just $ Rectangle (P $ round <$> (16 *) <$> position) (V2 16 16)
        x = xf + A.getKeyframeX frame
        y = yf + case direction of 
            A.South -> 0
            A.West -> 16
            A.East -> 32
            A.North -> 48


tileRect :: Num n => V2 Int -> Maybe (Rectangle n)
tileRect v = Just $ Rectangle (P $ fromIntegral <$> v) (V2 16 16)


drawGui :: MonadIO m => S.Sprites -> W.World -> m ()
drawGui sprites@ S.Sprites {S.renderer = renderer } world = do
    rendererDrawColor renderer $= V4 0 0 0 0xFF
    fillRect renderer $ Just $ Rectangle (P $ V2 480 0) (V2 640 480)
    rendererDrawColor renderer $= V4 0x40 0x40 0xC0 0xFF
    fillRect renderer $ Just $ Rectangle (P $ V2 528 sx) (V2 64 64)
    copyIcon (V2  0 128) (V2 544  16) (S.objects sprites)
    copyIcon (V2 96  64) (V2 544  64) (S.objects sprites)
    copyIcon (V2 64   0) (V2 544 112) (S.actors  sprites)
    copyIcon (V2 64  64) (V2 544 160) (S.objects sprites)
    copyIcon (V2  0  by) (V2 544 400) (S.buttons sprites)
    copy renderer (getAlgorithm sprites) Nothing 
        (Just $ Rectangle (P $ V2 528 208) (V2 64 32))
    where
        sx = case W.brush world of
            W.PathBrush  -> 0
            W.WallBrush  -> 48
            W.StartBrush -> 96 
            W.GoalBrush  -> 144

        by = case W.gameState world of
            W.Edit -> 0
            _ -> 16

        getAlgorithm = case W.algorithm world of
            W.BreadthFirstA -> S.breadth
            W.DijkstraA -> S.dijkstra
            W.GreedyBestA -> S.greedy
            W.AStarA -> S.aStar

        copyIcon s d tx = copy renderer tx
            (Just $ Rectangle (P s) (V2 16 16)) 
            (Just $ Rectangle (P d) (V2 32 32))
            
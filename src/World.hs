{-
-}

module World where


import qualified Actor as A
import Control.Monad
import Control.Monad.ST
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as W
import Linear


data World = World 
    { worldMap :: Layer Tile
    , explorer :: A.Actor
    , start :: V2 Int
    , goal :: V2 Int
    , brush :: Brush
    , algorithm :: Algorithm
    , gameState :: GameState
    }


data Layer a = Layer
    { layerSize :: V2 Int
    , layerArray :: V.MVector RealWorld a
    }


data Tile = Path | Wall
    deriving (Read, Show, Eq)


data Brush = PathBrush | WallBrush | StartBrush | GoalBrush
    deriving (Read, Show, Eq)


data GameState = 
      Edit 
    | BreadthFirst { frontier :: [V2 Int], cameFrom :: FreeExploration }
    | Dijkstra {costFrontier :: [(Float, V2 Int)], costFrom :: Checked Float }
    | GreedyBest {costFrontier :: [(Float, V2 Int)], costFrom :: Checked Float }
    | AStar {costFrontier :: [(Float, V2 Int)], checkFrom :: Checked (V2 Float) }
    | Found { visited :: V.Vector (V2 Int), path :: V.Vector (V2 Int) }
    | NoPath { visited :: V.Vector (V2 Int) }
    deriving (Read, Show, Eq)


data Algorithm =
      BreadthFirstA
    | DijkstraA
    | GreedyBestA
    | AStarA
    deriving (Read, Show, Eq)


makeLayer :: V2 Int -> a -> IO (Layer a)
makeLayer v@(V2 x y) a = Layer v <$> W.replicate (x * y) a


writeLayer :: Layer a -> V2 Int -> a -> IO ()
writeLayer Layer { layerArray = array, layerSize = V2 w h } (V2 x y) a 
    = when (0 <= x && x < w && 0 <= y && y < h) $ 
        W.write array (h * x + y) a


readLayer :: Layer a -> V2 Int -> IO (Maybe a) 
readLayer Layer { layerArray = array, layerSize = V2 w h } (V2 x y) 
    | 0 <= x && x < w && 0 <= y && y < h = Just <$> W.read array (h * x + y)
    | otherwise = return Nothing
    

type FreeExploration = M.Map (V2 Int) (Maybe (V2 Int))
type Checked a = M.Map (V2 Int) (a, Maybe (V2 Int))